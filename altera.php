<?php
error_reporting(E_ALL);
		ini_set('display_errors','on');
if(!empty($_GET) ){
	$id = $_GET ['id'];
		
		require_once ('database.php');
		$sql ='select * from usuarios where id=?';
		$consulta = $conexao->prepare($sql);
		$consulta->execute(array($id));
		$dados = $consulta->fetch(PDO::FETCH_ASSOC);	
}

if(!empty($_POST) ){
	$id = $_POST['id'];
	
	$nome = $_POST['nome'];
	$turma = $_POST['turma'];
	$email = $_POST['email'];
	$senha = $_POST['senha'];
	
	/*echo '<p>'.$id. '</p>';
	
	echo '<p>'.$nome. '</p>';
	
	echo '<p>'.$turma. '</p>';
	
	echo '<p>'.$email. '</p>';
	
	echo '<p>'.$senha. '</p>';*/
	
	
	$id = strip_tags (filter_var($id, FILTER_VALIDATE_INT));
	$nome = strip_tags (filter_var($nome, FILTER_SANITIZE_STRING));
	$turma = strip_tags (filter_var($turma, FILTER_SANITIZE_STRING));
	$email = strip_tags (filter_var($email, FILTER_SANITIZE_STRING));
	$senha = strip_tags (filter_var($senha, FILTER_SANITIZE_STRING));
	
		
		/*echo '<p>'.$id. '</p>';
	
	echo '<p>'.$nome. '</p>';
	
	echo '<p>'.$turma. '</p>';
	
	echo '<p>'.$email. '</p>';
	
	echo '<p>'.$senha. '</p>';*/
	
	
$valido= True;
if ($id == False or $id == null){
	$valido= False;
	}
	if ($nome == False or $nome == null){
	$valido= False;
	}
if ($turma == False or $turma == null){
	$valido= False;
	}
if ($email == False or $email == null){
	$valido= False;
	}
if ($senha == False or $senha == null){
	$valido= False;
	}
if($valido){
	require_once ('database.php');
		$sql ='UPDATE usuarios SET 
		nome = ?,
		turma = ?,
		email = ?,
		senha = ?
		WHERE id = ?';
		
		$alteracao = $conexao->prepare($sql);
		$ok = $alteracao->execute(
		array ($nome,$turma,$email,$senha,$id));
		} else {
			$ok = False;
}
			if ($ok){
				$msg= '<p class="alert alert-success" > Usuarios alterados com sucesso.  </p>';
				}else{
					$msg= '<p class="alert alert-danger" > Usuarios  não Alteradas!  </p>';
			}
			
		header('location:listagem.php?mensagem='.$msg);//redireciona para local especificado
}	
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Alteração</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
  
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
</head>

<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> 
					 <a class="navbar-brand" href="#">Meu Site</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li>
							<a href="listagem.php">LISTAGEM</a>
						</li>
						
						<li class="#">
							<a href="inseri.php">INSERI </a>
						</li>
						<li class="active">
							<a href="altera.php">ALTERA </a>
						</li>
						<li class="#">
							<a href="#">DELETA </a>
						</li>
						<li class="dropdown ">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">One more separated link</a>
								</li>
							</ul>
						</li>
					</ul>
					<form class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input class="form-control" type="text">
						</div> <button type="submit" class="btn btn-default">Submit</button>
					</form>
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="#">Link</a>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				
			</nav>
			<ul class="breadcrumb">
				<li>
					<a href="#">Home</a> <span class="divider">/</span>
				</li>
				<li>
					<a href="#">Library</a> <span class="divider">/</span>
				</li>
				<li class="active">
					Data
				</li>
			</ul>
		
			<div class="page-header">
			<p> Altera USUARIOS</p>	
			</div>
			<form class="form-horizontal" action= "altera.php" method="post">
				<input type = "hidden" name="id" value="<?php echo $id; ?>"/>
				<fieldset>
					<legend>Altera dados do usuario</legend>
			
					<div class="form-group">
						<label class="col-md-4 control-label" for="nome" >nome: </label>
						<div class="col-md-4">
							<input type = "text" name="nome" class="form-control input-md" value="<?php echo $dados['nome']; ?>"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="origem" >turma: </label>
						<div class="col-md-4">
							<input type = "text" name="turma" class="form-control input-md" value="<?php echo $dados['turma']; ?>"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="fundacao" >email: </label>
						<div class="col-md-4">
							<input type = "email" name="email" class="form-control input-md" value="<?php echo $dados['email']; ?>"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="presidente" >senha: </label>
						<div class="col-md-4">
							<input type = "password" name="senha" class="form-control input-md" value="<?php echo $dados['senha']; ?>"/>
						</div>
						
					</div>
					<div class="form-group text-center">
						<div class="col-md-8">
							<button type=submit class="btn btn-primary">Gravar </button> 
						</div>
					</div>		
				</fieldset>
			</form> 
				
		<hr/>
		<div class= "foot well">
		<P>&copy; 2015 -Billy </P>
			</p>
		</div>
	</div>
</div>
</body>
</html>